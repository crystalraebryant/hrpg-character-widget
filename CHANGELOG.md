# Change Log
All notable changes to this project will be documented in this file.

## [1.1] - 2015-12-11
### Added
- remove saved options on uninstall
- option to choose whether to display username
- change log

## [1.0] - 2015-12-10
### Added
- initial version of project