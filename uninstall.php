<?php

// If uninstall is not called from WordPress, exit
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit();
}

// Delete the option data saved for the widget
delete_option( 'widget_hrpg_character_widget' );

?>