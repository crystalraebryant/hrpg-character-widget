<?php
/**
 * Plugin Name: Habitica User Widget
 * Plugin URI: http://www.crystalraebryant.com/character-widget-for-habitica-wordpress-plugin/
 * Description: This plugin displays a user's Habitica character and stats in a widget.
 * Version: 1.1
 * Author: Crystal Rae Bryant
 * Author URI: http://www.crystalraebryant.com
 * License: GPLv2 or later
 */

include 'get-character.php';

// Creating the widget 
class hrpg_character_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'hrpg_character_widget', 

			// Widget name will appear in UI
			__('Habitica Character Widget', 'hrpg_character_widget_domain'), 

			// Widget description
			array( 'description' => __( 'Display your Habitica character and stats', 'hrpg_character_widget_domain' ), ) 
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		// Add the styles used by the widget
		wp_enqueue_style( 'hrpg-character-style', plugins_url('/inc/habitrpg.css">', __FILE__ ));
		wp_enqueue_style( 'hrpg-sprites-style', plugins_url('/inc/spritesmith.css">', __FILE__ ));
		wp_enqueue_style( 'lato-font-style', 'http://fonts.googleapis.com/css?family=Lato', __FILE__ );

		//Retrieve the saved title
		$title = apply_filters( 'widget_title', $instance['title'] );
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		$api_key = $instance[ 'api_key' ];
		$api_user_id = $instance[ 'api_user_id' ];
		$show_user_name = $instance[ 'show_user_name' ];


		// This is where you run the code and display the output
		get_character($api_user_id, $api_key, $show_user_name);
	}
			
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( '', 'hrpg_character_widget_domain' );
		}
		if ( isset( $instance[ 'api_user_id' ] ) ) {
			$api_user_id = $instance[ 'api_user_id' ];
		}
		if ( isset( $instance[ 'api_key' ] ) ) {
			$api_key = $instance[ 'api_key' ];
		}
		if ( isset( $instance[ 'show_user_name' ] ) ) {
			$show_user_name = $instance[ 'show_user_name' ];
		}
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		<label for="<?php echo $this->get_field_id( 'api_user_id' ); ?>"><?php _e( 'User ID:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'api_user_id' ); ?>" name="<?php echo $this->get_field_name( 'api_user_id' ); ?>" type="text" value="<?php echo esc_attr( $api_user_id ); ?>" />
		<label for="<?php echo $this->get_field_id( 'api_key' ); ?>"><?php _e( 'API Key:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'api_key' ); ?>" name="<?php echo $this->get_field_name( 'api_key' ); ?>" type="password" value="<?php echo esc_attr( $api_key ); ?>" />
		<label for="<?php echo $this->get_field_id( 'show_user_name' ); ?>"><?php _e( 'Show username?' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'show_user_name' ); ?>" name="<?php echo $this->get_field_name( 'show_user_name' ); ?>" type="checkbox" <?php if ($show_user_name == 'on') {echo 'checked="checked"';} ?> />
		</p>
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['api_user_id'] = strip_tags( $new_instance['api_user_id'] );
		$instance['api_key'] = strip_tags( $new_instance['api_key'] );
		$instance['show_user_name'] = $new_instance['show_user_name'];
		return $instance;
	}
} // Class hrpg_character_widget ends here

// Register and load the widget
function hrpg_character_load_widget() {
	register_widget( 'hrpg_character_widget' );
}
add_action( 'widgets_init', 'hrpg_character_load_widget' );
?>